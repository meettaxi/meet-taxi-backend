# «MeetTaxi - find a ride companion»

>  This is a repository of server side of application.


Application provides a service to find a taxi ride companion and save money.
You just need need to know the departure point and destination point, then search for an existing ride or create your own!
_______________________________________________________
Developed with [NestJS](https://nestjs.com/) framework.

Use SwaggerUI (/api path) to view documentation and models

*Project is not finished*
