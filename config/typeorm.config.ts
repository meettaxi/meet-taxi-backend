import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { IUser } from '../src/modules/user/iuser.entity';
import { IRide } from '../src/modules/ride/iride.entity';
import { IMessage } from '../src/modules/message/imessage.entity';
import { IRequest } from '../src/modules/request/irequest.entity';
import { Log } from '../src/modules/logger/log.entity';

export let typeOrmOptions: TypeOrmModuleOptions = {
  type: 'mysql',
  host: process.env.NODE_ENV === 'production' ? 'std-mysql' : 'localhost',
  port: 3306,
  username: 'std_247',
  password: 'qwerty123',
  database: process.env.NODE_ENV === 'production' ? 'std_247' : 'test',
  entities: [IUser, IRide, IMessage, IRequest, Log],
  synchronize: true,
};
