import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmOptions } from '../config/typeorm.config';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { MessageModule } from './modules/message/message.module';
import { RequestModule } from './modules/request/request.module';
import { LogModule } from './modules/logger/log.module';
import { LoggingInterceptor } from './modules/logger/interceptors/logging.interceptor';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmOptions),
    AuthModule,
    UserModule,
    MessageModule,
    RequestModule,
    LogModule,
  ],
  controllers: [AppController],
  providers: [LoggingInterceptor],
  exports: [LoggingInterceptor],
})
export class AppModule {
}
