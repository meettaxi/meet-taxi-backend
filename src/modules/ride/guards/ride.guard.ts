import { CanActivate, ExecutionContext, ForbiddenException, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Request } from 'express';
import { JwtPayload } from '../../auth/models/jwt-payload.model';
import { ExtractJwt } from 'passport-jwt';
import { AuthService } from '../../auth/auth.service';
import { RideService } from '../ride.service';
import { map } from 'rxjs/operators';
import { RideStatus } from '../../../common/models/enums';

@Injectable()
export class RideGuard implements CanActivate {
  constructor(private readonly authService: AuthService,
              private readonly rideService: RideService) {
  }

  canActivate(context: ExecutionContext): boolean | Observable<boolean> {
    const httpRequest: Request = context.switchToHttp().getRequest();
    const payload: JwtPayload = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(httpRequest));
    if (httpRequest.params.id && !isNaN(+httpRequest.params.id)) {
      return this.rideService.getById(+httpRequest.params.id)
        .pipe(
          map(iride => {
            if (iride.status !== RideStatus.OPEN) {
              throw new ForbiddenException('Ride status is not `OPEN` so it cannot be changed');
            }
            if (iride.createdBy.id === payload.sub) {
              return true;
            }
            throw new ForbiddenException('Only ride`s creator can make changes');
          }),
        );

    }
    return false;
  }
}
