import {
  BadRequestException,
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { RideService } from './ride.service';
import { ApiBearerAuth, ApiOkResponse, ApiUseTags } from '@nestjs/swagger';
import { CreateRideDto } from './dto/create-ride.dto';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Ride } from './models/ride.model';
import { UpdateRideDto } from './dto/update-ride.dto';
import { iRideToRide } from '../../common/utils';
import { GetAllRidesDto } from './dto/get-all-rides.dto';
import { AuthGuard } from '@nestjs/passport';
import { RideGuard } from './guards/ride.guard';
import { AuthService } from '../auth/auth.service';
import { ExtractJwt } from 'passport-jwt';
import { Request } from 'express';
import { SearchRideByViewportDto } from './dto/search-ride-by-viewport.dto';
import { LoggingInterceptor } from '../logger/interceptors/logging.interceptor';
import { SearchByAddressDto } from './dto/search-by-address.dto';

@ApiUseTags('ride')
@UseInterceptors(LoggingInterceptor)
@Controller('ride')
export class RideController {
  constructor(private readonly rideService: RideService, private readonly authService: AuthService) {
  }

  @Get('search')
  @ApiOkResponse({ type: Ride, isArray: true, description: 'An array of Rides with RideStatus=OPEN' })
  searchByBounds(@Query() searchOptions: SearchRideByViewportDto): Observable<Ride[]> {
    return this.rideService.searchByViewport(searchOptions);
  }

  @Get('search/address')
  searchByAddress(@Query() options: SearchByAddressDto): Observable<Ride[]> {
    if (!options.destinationAddr && !options.departureAddr) {
      throw new BadRequestException('You must specify at least `destinationAddress` or `departureAddress` to search');
    }
    return this.rideService.searchByDepartureDestinationAddress(
      options.departureAddr, options.destinationAddr, options.offset, options.limit)
      .pipe(
        catchError(err => {
          if (err.response) {
            throw new InternalServerErrorException(err.response);
          }
          throw new InternalServerErrorException(err);
        }),
      );
  }

  @Get(':id')
  @ApiOkResponse({ type: Ride })
  getById(@Param('id') id: number): Observable<Ride> {
    return this.rideService.getById(id);
  }

  @Put(':id')
  @ApiOkResponse({ type: Ride })
  @UseGuards(AuthGuard('jwt'), RideGuard)
  @ApiBearerAuth()
  update(@Param('id') id: number, @Body() options: UpdateRideDto): Observable<Ride> {
    return this.rideService.update(id, options);
  }

  @Post()
  @ApiOkResponse({ type: Ride })
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  create(@Body() createRideDto: CreateRideDto, @Req() req: Request): Observable<Ride> {
    const createdBy: number = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(req)).sub;
    return this.rideService.create({ ...createRideDto, createdBy })
      .pipe(
        catchError(err => {
          throw new InternalServerErrorException(err);
        }),
      );
  }

  @Get()
  @ApiOkResponse({ type: Ride, isArray: true, description: 'An array of Rides' })
  getAll(@Query() getAllOptions: GetAllRidesDto): Observable<Ride[]> {
    return this.rideService.getAll(getAllOptions)
      .pipe(
        map(irides => irides.map(iride => iRideToRide(iride))),
      );
  }

}
