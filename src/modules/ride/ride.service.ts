import { BadRequestException, forwardRef, Inject, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IRide } from './iride.entity';
import { Repository } from 'typeorm';
import { from, Observable, of } from 'rxjs';
import { IUser } from '../user/iuser.entity';
import { catchError, flatMap, map, tap } from 'rxjs/operators';
import { SimpleUser } from '../user/models/simple-user.model';
import { iRequestToRequest, iRideToRide, iUserToSimpleUser } from '../../common/utils';
import { Ride } from './models/ride.model';
import { UpdateRideDto } from './dto/update-ride.dto';
import { RideStatus, SortType } from '../../common/models/enums';
import {
  buildSearchParams,
  isDestinationPointEquals,
  isRideFinished,
  isRideInBounds,
  isRideStarted,
  rideIsNotBlocked,
} from './ride.helper';
import { GetAllRidesDto } from './dto/get-all-rides.dto';
import { Request } from '../request/models/request.model';
import { ICreateRideDto } from './dto/i-create-ride.dto';
import { MessageService } from '../message/message.service';
import { RIDE_FINISHED_SYS_MESSAGE, RIDE_STARTED_SYS_MESSAGE, SYSTEM_USER_PHONE } from '../../common/constants';
import { IMessage } from '../message/imessage.entity';
import { SearchRideByViewportDto } from './dto/search-ride-by-viewport.dto';

export const iRideRelations: string[] = ['createdBy', 'messages', 'requests', 'companions'];

@Injectable()
export class RideService {
  constructor(@InjectRepository(IRide) private readonly rideRepo: Repository<IRide>,
              @InjectRepository(IUser) private readonly userRepo: Repository<IUser>,
              @InjectRepository(IMessage) private readonly messageRepo: Repository<IMessage>,
              @Inject(forwardRef(() => MessageService)) private readonly messageService: MessageService) {
  }

  create(createRideOptions: ICreateRideDto): Observable<Ride> {
    return from(this.userRepo.findOne(createRideOptions.createdBy))
      .pipe(
        map(res => {
          if (!res) {
            throw new BadRequestException(`Can't find user by ID=${createRideOptions.createdBy} from field 'createdBy'`);
          }
          return res;
        }),
        flatMap(user => this.rideRepo.save(Object.assign(createRideOptions, { createdBy: user }))),
        flatMap(u => this.getById(u.id)),
      );
  }

  getById(id: number): Observable<Ride> {
    return from(this.rideRepo.findOneOrFail(id, { relations: iRideRelations }))
      .pipe(
        flatMap(iRide => this.updateStatus(iRide)),
        flatMap(ids => this.rideRepo.findOne(ids[0], { relations: iRideRelations })),
        map((iRide: IRide) => {
          const createdBy: SimpleUser = iUserToSimpleUser(iRide.createdBy);
          const companions = iRide.companions.map(usr => iUserToSimpleUser(usr));
          const requests: Request[] = iRide.requests.map(r => iRequestToRequest(r));
          const ride: Ride = Object.assign(iRide, { createdBy: createdBy, companions: companions, requests: requests });
          return ride;
        }),
        catchError(err => {
          if (err.name === 'EntityNotFound') {
            throw new NotFoundException(`No ride found with specified ID: ${id}`);
          }
          throw new InternalServerErrorException(err);
        }),
      );
  }

  update(id: number, updateOptions: UpdateRideDto): Observable<Ride> {
    return from(this.rideRepo.update(id, Object.assign(updateOptions, { updatedAt: new Date() })))
      .pipe(
        flatMap(() => {
          if (updateOptions.status && updateOptions.status === RideStatus.CANCELED) {
            return from(this.userRepo.findOne({ phone: SYSTEM_USER_PHONE }))
              .pipe(
                flatMap(usr => this.messageService.create({ ride: id, text: 'Поездка отменена создателем', author: usr.id })),
              );
          }
          return of();
        }),
        flatMap(() => this.getById(id)),
      );
  }

  getAll(options: GetAllRidesDto): Observable<IRide[]> {
    let parsedFilter: object;
    if (options.filter) {
      parsedFilter = JSON.parse(options.filter);
    }
    return from(this.rideRepo.find({
      order: { id: (options.sort || SortType.DESC as any) },
      skip: (options.offset && options.offset >= 0 ? options.offset : 0),
      take: (options.limit && options.limit >= 0 ? options.limit : 0),
      where: parsedFilter ? [parsedFilter] : undefined,
      relations: iRideRelations,
    }))
      .pipe(
        flatMap(irides => this.updateStatus(irides)),
        flatMap(ids => this.rideRepo.findByIds(ids, { relations: iRideRelations })),
        catchError(err => {
          throw new InternalServerErrorException(err);
        }),
        map(iRides => {
          if (iRides.length === 0) {
            throw new NotFoundException('No Rides found by given params. Try to change `offset` if it is not 0.');
          }
          return iRides;
        }),
      );
  }

  updateStatus(items: IRide | IRide[]): Observable<number[]> {
    if (!Array.isArray(items)) {
      items = [items];
    }
    const updatedRides: IRide[] = items.map(ride => {
      if (rideIsNotBlocked(ride)) {
        if (isRideStarted(ride)) {
          ride.status = RideStatus.STARTED;
        } else if (isRideFinished(ride)) {
          ride.status = RideStatus.FINISHED;
        }
      }
      return ride;
    });
    return from(this.rideRepo.save(updatedRides))
      .pipe(
        flatMap(rides => this.generateSystemMessages(rides)),
        flatMap(messages => this.messageRepo.save(messages)),
        map(() => (items as IRide[]).map(ride => ride.id)),
      );
  }

  searchByViewport(options: SearchRideByViewportDto): Observable<Ride[]> {
    const filterOptions: string = JSON.stringify({ status: RideStatus.OPEN });
    return this.getAll({ filter: filterOptions })
      .pipe(
        map(rides => rides.filter(rd => isRideInBounds(rd, options))),
        map(rides => options.destinationLatitude && options.destinationLongitude ?
          rides.filter(rd => isDestinationPointEquals(rd, options.destinationLatitude, options.destinationLongitude)) : rides),
      );
  }

  searchByDepartureDestinationAddress(departureAddr: string, destinationAddr: string, offset: number = 0, limit: number = 30)
    : Observable<Ride[]> {
    return from(this.rideRepo.find({
      skip: offset,
      take: limit,
      relations: iRideRelations,
      where: buildSearchParams(departureAddr, destinationAddr),
    }))
      .pipe(
        map(irides => {
          if (!irides || irides.length === 0) {
            throw new NotFoundException('No rides was found by given params');
          }
          return irides;
        }),
        map(irides => irides.map(iride => iRideToRide(iride))),
      );
  }

  private generateSystemMessages(rides: IRide[]): Observable<Partial<IMessage[]>> {
    const messages = [];
    return from(this.userRepo.findOne({ phone: SYSTEM_USER_PHONE }))
      .pipe(
        tap(sys => {
          rides.forEach(r => {
            if (r.status === RideStatus.STARTED && !r.messages.some(m => m.text === RIDE_STARTED_SYS_MESSAGE)) {
              messages.push({ ride: r, text: RIDE_STARTED_SYS_MESSAGE, author: sys.id });
            } else if (r.status === RideStatus.FINISHED && !r.messages.some(m => m.text === RIDE_FINISHED_SYS_MESSAGE)) {
              messages.push({ ride: r, text: RIDE_FINISHED_SYS_MESSAGE, author: sys.id });
            }
          });
        }),
        map(() => messages),
      );
  }
}
