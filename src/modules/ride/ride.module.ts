import { forwardRef, Module } from '@nestjs/common';
import { RideService } from './ride.service';
import { RideController } from './ride.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IRide } from './iride.entity';
import { IUser } from '../user/iuser.entity';
import { AuthModule } from '../auth/auth.module';
import { MessageModule } from '../message/message.module';
import { IMessage } from '../message/imessage.entity';
import { LogModule } from '../logger/log.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([IRide, IUser, IMessage]),
    AuthModule,
    forwardRef(() => MessageModule),
    LogModule,
  ],
  controllers: [RideController],
  providers: [RideService],
  exports: [RideService],
})
export class RideModule {
}
