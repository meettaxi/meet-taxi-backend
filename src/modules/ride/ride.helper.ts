import { Ride } from './models/ride.model';
import { IRide } from './iride.entity';
import { RideStatus } from '../../common/models/enums';
import { RIDE_FINISHED_INTERVAL } from '../../common/constants';
import { SearchRideByViewportDto } from './dto/search-ride-by-viewport.dto';
import { Like } from 'typeorm';

/**
 * Check if the minimum time interval from `departureDate` has passed
 * @param ride
 * @see RIDE_FINISHED_INTERVAL
 */
export function isRideFinished(ride: IRide | Ride) {
  return Date.now() - ride.departureDate.getTime() >= RIDE_FINISHED_INTERVAL;
}

/**
 * Check if it is possible to change any field of ride
 * @param ride
 */
export function rideIsNotBlocked(ride: IRide | Ride) {
  return ride.status === RideStatus.OPEN || ride.status === RideStatus.STARTED;
}

/**
 * Check if the ride is started and not finished yet
 * @param ride
 */
export function isRideStarted(ride: IRide | Ride) {
  return Date.now() - ride.departureDate.getTime() < RIDE_FINISHED_INTERVAL && ride.departureDate.getTime() < Date.now();
}

export function isRideInBounds(ride: Ride, searchOptions: SearchRideByViewportDto): boolean {
  return ride.departurePoint.latitude > searchOptions.minLatitude && ride.departurePoint.latitude < searchOptions.maxLatitude &&
    ride.departurePoint.longitude > searchOptions.minLongitude && ride.departurePoint.longitude < searchOptions.maxLongitude;
}

export function isDestinationPointEquals(ride: Ride, lat: number, lng: number): boolean {
  return +ride.destinationPoint.latitude.toFixed(5) === +lat.toFixed(5) &&
    +ride.destinationPoint.longitude.toFixed(5) === +lng.toFixed(5);
}

export function buildSearchParams(departureAddr: string, destinationAddr: string): object {
  const result: any = {};
  if (destinationAddr) {
    result.destinationAddress = Like(`%${destinationAddr}%`);
  }
  if (departureAddr) {
    result.departureAddress = Like(`%${departureAddr}%`);
  }
  return result;
}
