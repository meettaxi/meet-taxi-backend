import { Coordinates } from '../../../common/models/coordinates.model';
import { RideStatus } from '../../../common/models/enums';
import { SimpleUser } from '../../user/models/simple-user.model';
import { IMessage } from '../../message/imessage.entity';
import { ApiModelProperty } from '@nestjs/swagger';
import { Request } from '../../request/models/request.model';

export class Ride {
  @ApiModelProperty({ readOnly: true, required: true, type: 'integer', minimum: 1, example: 123, nullable: false })
  id: number;

  @ApiModelProperty({ required: true, type: Coordinates, example: { latitude: 12.44, longitude: -4.21 }, nullable: false })
  departurePoint: Coordinates;

  @ApiModelProperty({ required: true, type: Coordinates, example: { latitude: 12.455, longitude: -4.11 }, nullable: false })
  destinationPoint: Coordinates;

  @ApiModelProperty({ required: true, type: 'date', example: '2019-12-06T16:11:53.000Z', nullable: false })
  departureDate: Date;

  @ApiModelProperty({
    required: true,
    type: 'string',
    nullable: false,
    example: 'Россия, Москва, Театральный проезд, 3с1',
    description: 'Make sure to send a city, street and building number here.',
  })
  departureAddress: string;

  @ApiModelProperty({
    required: true,
    type: 'string',
    nullable: false,
    example: 'Россия, Москва, Мясницкая улица, 1',
    description: 'Make sure to send a country, city, street and building number here.',
  })
  destinationAddress: string;

  @ApiModelProperty({
    required: true,
    type: 'string',
    example: 'STARTED',
    enum: ['OPEN', 'STARTED', 'FINISHED', 'CANCELED'],
    default: 'OPEN',
  })
  status: RideStatus;

  @ApiModelProperty({
    readOnly: true,
    required: true,
    type: 'date',
    nullable: false,
    example: '2019-12-06T16:11:53.000Z',
    default: 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @ApiModelProperty({ required: true, type: 'string', example: 'No luggage for me', nullable: true, default: null })
  comment?: string;

  @ApiModelProperty({ type: Request, isArray: true })
  requests: Request[];

  @ApiModelProperty({ type: SimpleUser, isArray: true })
  companions: SimpleUser[];

  @ApiModelProperty({
    readOnly: true,
    required: true,
    type: SimpleUser,
    example: { id: 228, name: 'John', phone: 89095550011 },
    nullable: false,
  })
  createdBy: SimpleUser;

  @ApiModelProperty({ required: true, type: 'array', example: [], nullable: false })
  messages: IMessage[];
}
