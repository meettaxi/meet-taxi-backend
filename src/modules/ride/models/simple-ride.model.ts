import { RideStatus } from '../../../common/models/enums';
import { SimpleUser } from '../../user/models/simple-user.model';
import { ApiModelProperty } from '@nestjs/swagger';

export class SimpleRide {
  @ApiModelProperty({ type: 'integer', required: true, minimum: 1, nullable: false, example: 1 })
  id: number;

  @ApiModelProperty({ type: RideStatus, required: true, nullable: false, example: 'STARTED' })
  status: RideStatus;

  @ApiModelProperty({ type: 'date', required: true, nullable: false, example: '2020-01-02T16:11:53.000Z' })
  departureDate: Date;

  @ApiModelProperty({
    type: 'string',
    nullable: false,
    example: 'Россия, Москва, Театральный проезд, 3с1',
    description: 'Make sure to send a city, street and building number here.',
  })
  destinationAddress: string;

  @ApiModelProperty({
    type: 'string',
    nullable: false,
    example: 'Россия, Москва, Театральный проезд, 3с2',
    description: 'Make sure to send a city, street and building number here.',
  })
  departureAddress: string;

  @ApiModelProperty({
    type: SimpleUser, required: true, nullable: false, example: {
      id: 1, name: 'Ivan', phone: 88005553535,
    },
  })
  createdBy: SimpleUser;
}
