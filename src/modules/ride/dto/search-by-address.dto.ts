import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNumberString, IsOptional, IsString } from 'class-validator';

export class SearchByAddressDto {

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional({ type: 'string', nullable: true, example: 'Россия, Москва, Мясницкая, 1' })
  destinationAddr?: string;

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional({ type: 'string', nullable: true, example: 'Россия, Москва, Мясницкая, 2' })
  departureAddr?: string;

  @IsOptional()
  @IsNumberString()
  @ApiModelPropertyOptional({ type: 'integer', example: 5, nullable: true, default: 0 })
  offset?: number;

  @IsNumberString()
  @IsOptional()
  @ApiModelPropertyOptional({ type: 'integer', example: 10, nullable: true, default: 30, maximum: 100, minimum: 1 })
  limit?: number;
}
