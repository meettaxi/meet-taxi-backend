import { RideStatus } from '../../../common/models/enums';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { Coordinates } from '../../../common/models/coordinates.model';
import { IsDateString, IsEnum, IsNotEmpty, IsNotEmptyObject, IsOptional, IsString, MaxLength } from 'class-validator';
import { IsCoordinates } from '../../../common/decorators/is-coordinates';
import { IsDepartureDate } from '../../../common/decorators/is-departure-date';
import { MAX_RIDE_COMMENT_LENGTH } from '../../../common/constants';

export class UpdateRideDto {
  @IsNotEmptyObject()
  @IsCoordinates({ message: 'Latitude must be: [-90, 90], longitude must be: [-180, 180]' })
  @IsOptional()
  @ApiModelPropertyOptional({ type: Coordinates, nullable: false, example: { latitude: 12.55, longitude: -4.055 } })
  departurePoint?: Coordinates;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @ApiModelPropertyOptional({
    type: 'string',
    nullable: false,
    example: 'Россия, Москва, Мясницкая улица, 2',
    description: 'Make sure to send a country, city, street and building number here.',
  })
  departureAddress?: string;

  @IsDateString()
  @IsNotEmpty()
  @IsOptional()
  @IsDepartureDate({ message: 'departureDate MUST be at least > 12 hours from now' })
  @ApiModelPropertyOptional({ type: 'date', nullable: false, example: '2019-12-09T16:12:53.000Z' })
  departureDate?: Date;

  @IsOptional()
  @IsEnum(['CANCELED'], { message: 'You can only cancel Ride with status `OPEN`. Other options are not allowed.' })
  @ApiModelPropertyOptional({ type: 'string', enum: ['CANCELED'], nullable: false, example: 'CANCELED' })
  status?: RideStatus;

  @IsOptional()
  @IsString()
  @MaxLength(MAX_RIDE_COMMENT_LENGTH)
  @ApiModelPropertyOptional({ type: 'string', nullable: true, example: 'Will be with a small dog! 2 passengers are welcomed' })
  comment?: string;
}
