import { IsLatitude, IsLongitude, IsNotEmpty, IsOptional } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class SearchRideByViewportDto {
  @IsLatitude()
  @IsNotEmpty()
  @ApiModelProperty({
    type: 'number',
    nullable: false,
    required: true,
    minimum: -90,
    maximum: 90,
    description: 'Upper bound of departurePoint latitude. Must be > `minLatitude`',
  })
  readonly maxLatitude: number;

  @IsLongitude()
  @IsNotEmpty()
  @ApiModelProperty({
    type: 'number',
    nullable: false,
    required: true,
    minimum: -180,
    maximum: 180,
    description: 'Upper bound of departurePoint longitude. Must be > `minLongitude`',
  })
  readonly maxLongitude: number;

  @IsLatitude()
  @IsNotEmpty()
  @ApiModelProperty({
    type: 'number',
    nullable: false,
    required: true,
    minimum: -90,
    maximum: 90,
    description: 'Lower bound of departurePoint latitude. Must be < `maxLatitude`',
  })
  readonly minLatitude: number;

  @IsLongitude()
  @IsNotEmpty()
  @ApiModelProperty({
    type: 'number',
    nullable: false,
    required: true,
    minimum: -180,
    maximum: 180,
    description: 'Lower bound of departurePoint latitude. Must be < `maxLongitude`',
  })
  readonly minLongitude: number;

  @IsOptional()
  @IsLongitude()
  @ApiModelPropertyOptional({
    type: 'number',
    minimum: -180,
    maximum: 180,
    description: 'Destination point longitude. Precision - 5 digits after comma.',
  })
  readonly destinationLongitude?: number;

  @IsLatitude()
  @IsOptional()
  @ApiModelPropertyOptional({
    type: 'number',
    minimum: -180,
    maximum: 180,
    description: 'Destination point latitude. Precision - 5 digits after comma.',
  })
  readonly destinationLatitude?: number;
}
