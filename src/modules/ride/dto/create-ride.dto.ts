import { Coordinates } from '../../../common/models/coordinates.model';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsDateString, IsInt, IsNotEmpty, IsNotEmptyObject, IsOptional, IsString, Max, MaxLength, Min } from 'class-validator';
import { IsCoordinates } from '../../../common/decorators/is-coordinates';
import { IsDepartureDate } from '../../../common/decorators/is-departure-date';
import { MAX_RIDE_COMMENT_LENGTH, MAX_RIDE_COMPANIONS, MIN_RIDE_COMPANIONS } from '../../../common/constants';

export class CreateRideDto {
  @IsNotEmptyObject()
  @IsCoordinates({ message: 'Latitude must be: [-90, 90], longitude must be: [-180, 180]' })
  @ApiModelProperty({ required: true, type: Coordinates, nullable: false, example: { latitude: 44.22456, longitude: 22.55768 } })
  departurePoint: Coordinates;

  @IsNotEmpty()
  @IsString()
  @ApiModelProperty({
    required: true,
    type: 'string',
    nullable: false,
    example: 'Россия, Москва, Театральный проезд, 3с2',
    description: 'Make sure to send a city, street and building number here.',
  })
  departureAddress: string;

  @IsNotEmptyObject()
  @IsCoordinates({ message: 'Latitude must be: [-90, 90], longitude must be: [-180, 180]' })
  @ApiModelProperty({ required: true, type: Coordinates, nullable: false, example: { latitude: 45.12098, longitude: 23.05 } })
  destinationPoint: Coordinates;

  @IsNotEmpty()
  @IsString()
  @ApiModelProperty({
    required: true,
    type: 'string',
    nullable: false,
    example: 'Россия, Москва, Мясницкая улица, 1',
    description: 'Make sure to send a country, city, street and building number here.',
  })
  destinationAddress: string;

  @IsDateString()
  @IsNotEmpty()
  @IsDepartureDate({ message: 'departureDate MUST be at least > 12 hours from now' })
  @ApiModelProperty({
    required: true,
    type: Date,
    nullable: false,
    example: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 2),
    description: 'Date and time of departure, must be at least > 12 hours from now',
  })
  departureDate: Date;

  @IsInt()
  @Min(1)
  @Max(3)
  @ApiModelProperty({
    required: false,
    nullable: false,
    description: 'Maximum number of companions',
    type: 'integer',
    minimum: MIN_RIDE_COMPANIONS,
    maximum: MAX_RIDE_COMPANIONS,
    example: 1,
    default: 2,
  })
  maxCompanions: number;

  @IsOptional()
  @IsString()
  @MaxLength(MAX_RIDE_COMMENT_LENGTH)
  @ApiModelProperty({
    type: 'string',
    required: false,
    maxLength: MAX_RIDE_COMMENT_LENGTH,
    nullable: true,
    description: 'Any additional info about ride',
    example: 'Буду с собакой',
  })
  comment?: string;
}
