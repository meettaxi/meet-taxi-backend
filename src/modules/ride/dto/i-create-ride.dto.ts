import { IsNumber, Min } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateRideDto } from './create-ride.dto';

/**
 * Internal model for CreateRide
 */
export class ICreateRideDto extends CreateRideDto {
  @IsNumber()
  @Min(1)
  @ApiModelProperty({ required: true, nullable: false, type: 'number', example: 1 })
  createdBy: number;
}
