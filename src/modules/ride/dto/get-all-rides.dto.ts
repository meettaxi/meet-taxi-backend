import { GetAllOptions } from '../../../common/models/get-all-options.model';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsJSON, IsOptional } from 'class-validator';

export class GetAllRidesDto extends GetAllOptions {
  @IsJSON()
  @IsOptional()
  @ApiModelPropertyOptional({
    type: 'JSON',
    default: null,
    example: { filter: { id: 1 } },
    nullable: true,
    description: 'JSON string with fields you want to filter, example: {"status":"OPEN"}',
  })
  filter: string;
}
