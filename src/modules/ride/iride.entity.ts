import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { IMessage } from '../message/imessage.entity';
import { RideStatus } from '../../common/models/enums';
import { Coordinates } from '../../common/models/coordinates.model';
import { IUser } from '../user/iuser.entity';
import { IRequest } from '../request/irequest.entity';

/**
 * Ride internal model
 */
@Entity()
export class IRide {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @Column({ type: 'json', nullable: false })
  departurePoint: Coordinates;

  @Column({ type: 'varchar', nullable: false })
  departureAddress: string;

  @Column({ type: 'json', nullable: false })
  destinationPoint: Coordinates;

  @Column({ type: 'varchar', nullable: false })
  destinationAddress: string;

  @Column({ type: 'timestamp', nullable: false })
  departureDate: Date;

  @Column({ type: 'enum', enum: RideStatus, default: RideStatus.OPEN })
  status: RideStatus;

  @Column({ type: 'timestamp', nullable: false, default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @Column({ type: 'timestamp', nullable: true, default: null })
  updatedAt: Date;

  @Column({ type: 'varchar', nullable: true, default: null })
  comment?: string;

  @Column({ type: 'tinyint', nullable: false, default: 2, unsigned: true })
  maxCompanions: number;

  @OneToMany(type => IRequest, request => request.ride)
  requests: IRequest[];

  @ManyToMany(type => IUser, iuser => iuser.participatedRides, { cascade: true })
  @JoinTable()
  companions: IUser[];

  @ManyToOne(type => IUser, user => user.createdRides, { eager: true })
  createdBy: IUser;

  @OneToMany(type => IMessage, message => message.ride)
  messages: IMessage[];
}
