import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IMessage } from './imessage.entity';
import { MessageController } from './message.controller';
import { RideModule } from '../ride/ride.module';
import { AuthModule } from '../auth/auth.module';
import { LogModule } from '../logger/log.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([IMessage]),
    RideModule,
    AuthModule,
    LogModule,
  ],
  controllers: [MessageController],
  providers: [MessageService],
  exports: [MessageService],
})
export class MessageModule {
}
