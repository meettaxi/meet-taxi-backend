import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IMessage } from './imessage.entity';
import { Repository } from 'typeorm';
import { from, Observable } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';
import { RideService } from '../ride/ride.service';
import { iMessageToMessage } from '../../common/utils';
import { Message } from './models/message.model';
import { UpdateMessageDto } from './dto/update-message.dto';
import { ICreateMessageDto } from './dto/i-create-message.dto';

@Injectable()
export class MessageService {
  constructor(@InjectRepository(IMessage) private readonly messageRepo: Repository<IMessage>,
              private readonly rideService: RideService) {
  }

  getById(id: number): Observable<Message> {
    return from(this.messageRepo.findOneOrFail(id, { relations: ['author', 'ride'] }))
      .pipe(
        map(m => iMessageToMessage(m)),
      );
  }

  create(options: ICreateMessageDto): Observable<Message> {
    return this.rideService.getById(options.ride)
      .pipe(
        flatMap(iride => {
          const opts = Object.assign(options, { ride: iride });
          return this.messageRepo.save(opts);
        }),
        flatMap((m: Message) => this.getById(m.id)),
      );
  }

  update(id: number, options: UpdateMessageDto): Observable<Message> {
    return from(this.messageRepo.update(id, { text: options.text, isEdited: true }))
      .pipe(
        flatMap(() => this.getById(id)),
      );
  }
}
