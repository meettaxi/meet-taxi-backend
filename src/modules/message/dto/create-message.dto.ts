import { ApiModelProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsString, MaxLength, Min } from 'class-validator';
import { MAX_MESSAGE_TEXT_LENGTH } from '../../../common/constants';

export class CreateMessageDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(MAX_MESSAGE_TEXT_LENGTH)
  @ApiModelProperty({
    required: true,
    type: 'string',
    maxLength: MAX_MESSAGE_TEXT_LENGTH,
    example: 'How much luggage do you have?',
    nullable: false,
  })
  readonly text: string;

  @IsInt()
  @IsNotEmpty()
  @Min(1)
  @ApiModelProperty({ required: true, type: 'integer', nullable: false, minimum: 1, example: 1 })
  readonly ride: number;
}
