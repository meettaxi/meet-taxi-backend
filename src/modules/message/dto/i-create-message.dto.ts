/**
 * Internal model for CreateMessage
 */
import { CreateMessageDto } from './create-message.dto';
import { IsInt, IsNotEmpty, Min } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ICreateMessageDto extends CreateMessageDto {
  @IsInt()
  @IsNotEmpty()
  @Min(1)
  @ApiModelProperty({ required: true, type: 'integer', nullable: false, minimum: 1, example: 1 })
  readonly author: number;
}
