import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { MAX_MESSAGE_TEXT_LENGTH } from '../../../common/constants';

export class UpdateMessageDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(MAX_MESSAGE_TEXT_LENGTH)
  @ApiModelProperty({
    required: true,
    type: 'string',
    maxLength: MAX_MESSAGE_TEXT_LENGTH,
    example: 'How much luggage do you have?',
    nullable: false,
  })
  readonly text: string;
}
