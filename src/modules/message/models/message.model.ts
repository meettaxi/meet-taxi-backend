import { SimpleUser } from '../../user/models/simple-user.model';
import { SimpleRide } from '../../ride/models/simple-ride.model';
import { ApiModelProperty } from '@nestjs/swagger';

export class Message {
  @ApiModelProperty({ type: 'integer', required: true, minimum: 1, nullable: false, example: 1 })
  id: number;

  @ApiModelProperty({ type: 'string', required: true, nullable: false, example: 'Any luggage?' })
  text: string;

  @ApiModelProperty({ type: 'date', required: true, nullable: false, example: '2019-12-01T16:11:53.000Z' })
  postedAt: Date;

  @ApiModelProperty({
    type: SimpleRide, nullable: false, required: true, example: {
      id: 1,
      status: 'OPEN',
      departureDate: '2019-12-12T16:11:53.000Z',
      createdBy: {
        id: 1, name: 'Ivan', phone: 88005553535,
      },
    },
  })
  ride: SimpleRide;

  @ApiModelProperty({
    type: SimpleUser, nullable: false, required: true, example: {
      id: 1, name: 'Ivan', phone: 88005553535,
    },
  })
  author: SimpleUser;

  @ApiModelProperty({
    type: 'boolean',
    nullable: false,
    required: true,
    example: false,
    description: 'Indicates if the original message was edited by author',
  })
  isEdited: boolean;
}
