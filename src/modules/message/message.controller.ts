import {
  BadRequestException,
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { Observable } from 'rxjs';
import { Message } from './models/message.model';
import { ApiBearerAuth, ApiCreatedResponse, ApiImplicitBody, ApiOkResponse, ApiUseTags } from '@nestjs/swagger';
import { catchError } from 'rxjs/operators';
import { UpdateMessageDto } from './dto/update-message.dto';
import { AuthGuard } from '@nestjs/passport';
import { CreateMessageGuard } from './guards/create-message.guard';
import { UpdateMessageGuard } from './guards/update-message.guard';
import { Request } from 'express';
import { AuthService } from '../auth/auth.service';
import { ExtractJwt } from 'passport-jwt';
import { ICreateMessageDto } from './dto/i-create-message.dto';
import { LoggingInterceptor } from '../logger/interceptors/logging.interceptor';

@ApiUseTags('message')
@UseInterceptors(LoggingInterceptor)
@Controller('message')
export class MessageController {
  constructor(private readonly messageService: MessageService, private readonly authService: AuthService) {
  }

  @Post()
  @UseGuards(AuthGuard('jwt'), CreateMessageGuard)
  @ApiImplicitBody({ type: CreateMessageDto, required: true, name: 'CreateMessageDto' })
  @ApiBearerAuth()
  @ApiCreatedResponse({ type: Message, description: 'Created message in specified ride' })
  create(@Body() createMessageDto: CreateMessageDto, @Req() req: Request): Observable<Message> {
    const author: number = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(req)).sub;
    const options: ICreateMessageDto = { ...createMessageDto, author };
    return this.messageService.create(options);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiOkResponse({ type: Message })
  @ApiBearerAuth()
  getById(@Param('id') id: number): Observable<Message> {
    return this.messageService.getById(id);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'), UpdateMessageGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: Message, description: 'Edited message. `isEdited` field will be set to `true`' })
  updateById(@Param('id') id: number, @Body() body: UpdateMessageDto): Observable<Message> {
    if (!body.text) {
      throw new BadRequestException('Message text must contain at least 1 symbol');
    }
    return this.messageService.update(id, { text: body.text })
      .pipe(
        catchError(err => {
          throw new InternalServerErrorException(err);
        }),
      );
  }
}
