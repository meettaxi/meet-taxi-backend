import { CanActivate, ExecutionContext, ForbiddenException, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { Request } from 'express';
import { JwtPayload } from '../../auth/models/jwt-payload.model';
import { ExtractJwt } from 'passport-jwt';
import { map } from 'rxjs/operators';
import { RideService } from '../../ride/ride.service';
import { SimpleUser } from '../../user/models/simple-user.model';
import { RideStatus } from '../../../common/models/enums';

@Injectable()
export class CreateMessageGuard implements CanActivate {
  constructor(private readonly authService: AuthService,
              private readonly rideService: RideService) {
  }

  canActivate(context: ExecutionContext): boolean | Observable<boolean> {
    const httpRequest: Request = context.switchToHttp().getRequest();
    const payload: JwtPayload = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(httpRequest));
    return this.rideService.getById(+httpRequest.body.ride)
      .pipe(
        map(ride => {
          if ((ride.createdBy.id === payload.sub || this.isUserRideCompanion(payload.sub, ride.companions))
            && ride.status !== RideStatus.FINISHED) {
            return true;
          }
          if (ride.status === RideStatus.FINISHED) {
            throw new ForbiddenException('Ride status must not be `FINISHED` to write messages.');
          }
          throw new ForbiddenException('Only ride members can write messages to chat.');
        }),
      );
  }

  private isUserRideCompanion(userId: number, companions: SimpleUser[]): boolean {
    return companions.some(user => user.id === userId);
  }
}
