import { CanActivate, ExecutionContext, ForbiddenException, Injectable } from '@nestjs/common';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';
import { Request } from 'express';
import { JwtPayload } from '../../auth/models/jwt-payload.model';
import { ExtractJwt } from 'passport-jwt';
import { MessageService } from '../message.service';
import { map } from 'rxjs/operators';

@Injectable()
export class UpdateMessageGuard implements CanActivate {
  constructor(private readonly authService: AuthService,
              private readonly messageService: MessageService) {
  }

  canActivate(context: ExecutionContext): boolean | Observable<boolean> {
    const httpRequest: Request = context.switchToHttp().getRequest();
    const payload: JwtPayload = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(httpRequest));
    if (httpRequest.params.id && !isNaN(+httpRequest.params.id)) {
      return this.messageService.getById(+httpRequest.params.id)
        .pipe(
          map(m => {
            if (m.author.id === payload.sub) {
              return true;
            }
            throw new ForbiddenException(`You can edit only your own messages`);
          }),
        );
    }
    return false;
  }
}
