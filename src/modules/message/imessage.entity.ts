import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { IRide } from '../ride/iride.entity';
import { IUser } from '../user/iuser.entity';

@Entity()
export class IMessage {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @Column({ type: 'varchar', nullable: false })
  text: string;

  @Column({ type: 'timestamp', nullable: false, default: () => 'CURRENT_TIMESTAMP' })
  postedAt: Date;

  @ManyToOne(type => IRide, ride => ride.messages)
  ride: IRide;

  @ManyToOne(type => IUser, user => user.messages)
  author: IUser;

  @Column({ type: 'boolean', nullable: false, default: false })
  isEdited: boolean;
}
