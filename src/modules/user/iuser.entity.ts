import { Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UserStatus } from '../../common/models/enums';
import { IRide } from '../ride/iride.entity';
import { Rating } from './models/rating.model';
import { IMessage } from '../message/imessage.entity';
import { IRequest } from '../request/irequest.entity';

/**
 * User internal model
 */
@Entity()
export class IUser {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @Column({ type: 'varchar', nullable: false })
  name: string;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ type: 'varchar', nullable: true })
  email?: string;

  @Column({ type: 'bigint', nullable: false, unsigned: true, unique: true })
  phone: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', nullable: false })
  createdAt: Date;

  @Column({ type: 'timestamp', nullable: true, default: null })
  updatedAt?: Date;

  @Column({ type: 'timestamp', nullable: true, default: null })
  deletedAt?: Date;

  @Column({ type: 'json', nullable: true })
  rating?: Rating;

  @Column({ type: 'enum', enum: UserStatus, nullable: false, default: UserStatus.ACTIVE })
  status: UserStatus;

  @ManyToMany(type => IRide, ride => ride.companions)
  participatedRides: IRide[];

  @OneToMany(type => IRide, ride => ride.createdBy, { onDelete: 'CASCADE' })
  createdRides: IRide[];

  @OneToMany(type => IRequest, request => request.author)
  requests: IRequest[];

  @OneToMany(type => IMessage, message => message.author)
  messages: IMessage[];
}
