import { IsEmail, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString, Max, MaxLength, Min, MinLength } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { UserStatus } from '../../../common/models/enums';
import { MAX_NAME_LENGTH, MAX_PASSWORD_LENGTH, MIN_NAME_LENGTH, MIN_PASSWORD_LENGTH } from '../../../common/constants';

export class UpdateUserDto {
  @IsString()
  @IsOptional()
  @MinLength(MIN_NAME_LENGTH)
  @MaxLength(MAX_NAME_LENGTH)
  @ApiModelPropertyOptional({ type: 'string', nullable: false, minLength: MIN_NAME_LENGTH, maxLength: MAX_NAME_LENGTH, example: 'Ivan' })
  readonly name?: string;

  @IsString()
  @MinLength(MIN_PASSWORD_LENGTH)
  @MaxLength(MAX_PASSWORD_LENGTH)
  @IsOptional()
  @ApiModelPropertyOptional({ type: 'string', nullable: false, minLength: MIN_PASSWORD_LENGTH, example: 'qwerty123' })
  readonly password?: string;

  @IsEmail()
  @IsNotEmpty()
  @IsOptional()
  @ApiModelPropertyOptional({ type: 'string', nullable: false, minLength: 3, example: 'ivan@mail.com' })
  readonly email?: string;

  @IsInt()
  @IsOptional()
  @Min(1)
  @Max(5)
  @ApiModelPropertyOptional({ type: 'integer', nullable: false, minimum: 1, maximum: 5, example: 4 })
  readonly rating?: number;

  @IsEnum(Object.values(UserStatus))
  @IsNotEmpty()
  @IsOptional()
  @ApiModelPropertyOptional({ type: UserStatus, nullable: false, example: 'BANNED', enum: Object.values(UserStatus) })
  readonly status?: UserStatus;
}
