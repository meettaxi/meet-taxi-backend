import { IsNotEmpty, IsNumber, IsString, MaxLength, Min, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { MAX_NAME_LENGTH, MAX_PASSWORD_LENGTH, MIN_NAME_LENGTH, MIN_PASSWORD_LENGTH } from '../../../common/constants';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(MIN_NAME_LENGTH)
  @MaxLength(MAX_NAME_LENGTH)
  @ApiModelProperty({
    type: 'string',
    required: true,
    minLength: MIN_NAME_LENGTH,
    maxLength: MAX_NAME_LENGTH,
    nullable: false,
    description: 'First name of a new user',
    example: 'Roman',
  })
  readonly name: string;

  @IsNumber()
  @IsNotEmpty()
  @Min(100)
  @ApiModelProperty({ type: 'number', required: true, minimum: 100, nullable: false, example: 88005553535 })
  readonly phone: number;

  @IsString()
  @IsNotEmpty()
  @MinLength(MIN_PASSWORD_LENGTH)
  @MaxLength(MAX_PASSWORD_LENGTH)
  @ApiModelProperty({
    type: 'string',
    required: true,
    minLength: MIN_PASSWORD_LENGTH,
    maxLength: MAX_PASSWORD_LENGTH,
    nullable: false,
    example: 'QwErTy123',
  })
  readonly password: string;
}
