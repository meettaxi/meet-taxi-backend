import { UserStatus } from '../../../common/models/enums';
import { ApiModelProperty } from '@nestjs/swagger';
import { Ride } from '../../ride/models/ride.model';
import { Request } from '../../request/models/request.model';
import { Message } from '../../message/models/message.model';
import { MAX_NAME_LENGTH, MAX_PASSWORD_LENGTH, MIN_NAME_LENGTH, MIN_PASSWORD_LENGTH } from '../../../common/constants';

/**
 * User output model (with converted rating)
 */
export class User {
  @ApiModelProperty({ required: true, type: 'integer', nullable: false, minimum: 1, example: 1337 })
  id: number;

  @ApiModelProperty({ type: 'string', nullable: false, minLength: MIN_NAME_LENGTH, maxLength: MAX_NAME_LENGTH, example: 'Ivan' })
  name: string;

  @ApiModelProperty({
    type: 'string',
    nullable: false,
    minLength: MIN_PASSWORD_LENGTH,
    maxLength: MAX_PASSWORD_LENGTH,
    example: 'qwerty123',
  })
  password: string;

  @ApiModelProperty({ type: 'string', nullable: true, example: 'ivan@mail.com', default: null })
  email?: string;

  @ApiModelProperty({ type: 'bigint', nullable: false, example: 88005553535 })
  phone: number;

  @ApiModelProperty({
    type: Ride,
    isArray: true,
  })
  participatedRides: Ride[];

  @ApiModelProperty({ type: Ride, isArray: true })
  createdRides: Ride[];

  @ApiModelProperty({ type: Request, isArray: true })
  requests: Request[];

  @ApiModelProperty({ type: Message, isArray: true })
  messages: Message[];

  @ApiModelProperty({ type: 'date', nullable: false, example: '2019-12-01T16:11:53.000Z', default: 'CURRENT_TIMESTAMP', readOnly: true })
  createdAt: Date;

  @ApiModelProperty({ type: 'date', nullable: true, example: '2019-12-02T16:11:53.000Z', default: null })
  updatedAt?: Date;

  @ApiModelProperty({ type: 'date', nullable: true, example: '2019-12-03T16:11:53.000Z', default: null })
  deletedAt?: Date;

  @ApiModelProperty({ type: 'float', nullable: true, example: '4.51', default: null })
  rating?: number;

  @ApiModelProperty({ type: 'string', nullable: false, example: 'BANNED', enum: Object.values(UserStatus), default: 'ACTIVE' })
  status: UserStatus;
}
