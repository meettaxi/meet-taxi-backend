import { ApiModelProperty } from '@nestjs/swagger';
import { MAX_NAME_LENGTH, MIN_NAME_LENGTH } from '../../../common/constants';

export class SimpleUser {
  @ApiModelProperty({ type: 'integer', nullable: false, required: true, minimum: 1 })
  id: number;

  @ApiModelProperty({ type: 'string', nullable: false, minLength: MIN_NAME_LENGTH, maxLength: MAX_NAME_LENGTH })
  name: string;

  @ApiModelProperty({ type: 'bigint', nullable: false, minimum: 100 })
  phone: number;
}
