export interface Rating {
  marksCount: number;
  sum: number;
}
