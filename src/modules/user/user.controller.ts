import {
  Body,
  Controller,
  Delete,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBearerAuth, ApiCreatedResponse, ApiImplicitQuery, ApiOkResponse, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './models/user.model';
import { catchError, map } from 'rxjs/operators';
import { iUserToUser } from '../../common/utils';
import { DeleteResult } from '../../common/models/delete-result.model';
import { Observable, of } from 'rxjs';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from '@nestjs/passport';
import { MyselfGuard } from '../auth/guards/myself.guard';
import { LoggingInterceptor } from '../logger/interceptors/logging.interceptor';

@ApiUseTags('user')
@UseInterceptors(LoggingInterceptor)
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiResponse({ status: 200, type: User })
  getById(@Param('id') id: number): Observable<User> {
    return this.userService.getById(id)
      .pipe(
        map(iuser => iUserToUser(iuser)),
        catchError(err => {
          if (err.name === 'EntityNotFound') {
            throw new NotFoundException(`User with ID ${id} was not found`);
          }
          throw new InternalServerErrorException(err);
        }),
      );
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'), MyselfGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: User })
  update(@Param('id') id: number, @Body() updateOptions: UpdateUserDto): Observable<User> {
    return this.userService.update(id, updateOptions)
      .pipe(
        map(iuser => iUserToUser(iuser)),
        catchError(err => {
          if (err.name === 'EntityNotFound') {
            throw new NotFoundException(`User with ID ${id} was not found`);
          }
          throw new InternalServerErrorException(err);
        }),
      );
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'), MyselfGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: DeleteResult })
  delete(@Param('id') id: number): Observable<DeleteResult> {
    return this.userService.delete(id);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitQuery({ type: Number, name: 'phone', required: true })
  @ApiOkResponse({ type: User })
  getByPhone(@Query('phone') phone: number): Observable<User> {
    return this.userService.getByPhone(phone)
      .pipe(
        map(iuser => iUserToUser(iuser)),
      );
  }

  @Post()
  @ApiCreatedResponse({ type: User })
  create(@Body() options: CreateUserDto): Observable<User> {
    return this.userService.create(options)
      .pipe(
        map(iuser => iUserToUser(iuser)),
        catchError(err => {
          if (err.response) {
            return of(err.response);
          }
          throw new InternalServerErrorException(err);
        }),
      );
  }
}
