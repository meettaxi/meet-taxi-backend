import { forwardRef, Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IUser } from './iuser.entity';
import { AuthModule } from '../auth/auth.module';
import { LogModule } from '../logger/log.module';

@Module({
  imports: [TypeOrmModule.forFeature([IUser]), forwardRef(() => AuthModule), LogModule],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {
}
