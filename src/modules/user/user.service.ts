import {
  ConflictException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  OnApplicationBootstrap,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IUser } from './iuser.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { from, Observable, Subscription } from 'rxjs';
import { DeleteResult } from '../../common/models/delete-result.model';
import { catchError, filter, finalize, flatMap, map, tap } from 'rxjs/operators';
import { CreateUserDto } from './dto/create-user.dto';
import * as bcrypt from 'bcryptjs';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserStatus } from '../../common/models/enums';
import { addRatingMark } from '../../common/utils';
import { SYSTEM_USER_PHONE } from '../../common/constants';

const SELECT_USER_FIELDS: Array<keyof IUser> = ['id', 'name', 'email', 'phone', 'createdAt', 'updatedAt', 'deletedAt', 'rating', 'status'];

@Injectable()
export class UserService implements OnApplicationBootstrap {
  constructor(@InjectRepository(IUser) private readonly userRepo: Repository<IUser>) {
  }

  create(userCreateOptions: CreateUserDto): Observable<IUser> {
    return from(this.userRepo.findOne({ phone: userCreateOptions.phone }))
      .pipe(
        flatMap(res => {
          if (res) {
            throw new ConflictException(`User with phone ${userCreateOptions.phone} is already existing`);
          }
          if (userCreateOptions.name.trim().toLowerCase() === 'система') {
            throw new ForbiddenException(`You cannot register account with such a name: ${userCreateOptions.name}`);
          }
          return from(bcrypt.hash(userCreateOptions.password, 5));
        }),
        flatMap(hash => this.userRepo.save(Object.assign(userCreateOptions, { password: hash }))),
        flatMap(iuser => this.getById(iuser.id)),
      );
  }

  update(id: number, updateUserOptions: UpdateUserDto): Observable<IUser> {
    const options: Partial<IUser> = {};
    return from(this.userRepo.findOneOrFail(id))
      .pipe(
        tap(iuser => {
          if (updateUserOptions.rating) {
            options.rating = addRatingMark(iuser.rating, updateUserOptions.rating);
          }
          // build options object by copying non-empty props
          Object.keys(updateUserOptions).forEach(key => {
            if (updateUserOptions[key] && key !== 'id' && key !== 'password' && key !== 'rating') {
              options[key] = updateUserOptions[key];
            }
          });
        }),
        flatMap(async () => {
          if (updateUserOptions.password) {
            return bcrypt.hash(updateUserOptions.password, 5);
          }
          return '';
        }),
        flatMap(pass => {
          if (pass) {
            options.password = pass;
          }
          options.updatedAt = new Date();
          return this.userRepo.update(id, options);
        }),
        flatMap(() => this.getById(id)),
      );
  }

  getById(id: number): Observable<IUser> {
    return from(this.userRepo.findOneOrFail(id,
      { select: SELECT_USER_FIELDS, relations: ['messages', 'createdRides', 'participatedRides', 'requests'] }));
  }

  getByPhone(phone: number, withPassword?: boolean): Observable<IUser> {
    const options: FindOneOptions = {
      select: SELECT_USER_FIELDS, relations: ['messages', 'createdRides', 'participatedRides', 'requests'],
    };
    return from(this.userRepo.findOneOrFail({ phone: phone }, withPassword ? null : options))
      .pipe(
        catchError(err => {
          if (err.name === 'EntityNotFound') {
            throw new NotFoundException(`No user found with specified phone: ${phone}`);
          }
          throw new InternalServerErrorException(err);
        }),
      );
  }

  delete(id: number): Observable<DeleteResult> {
    return from(this.userRepo.findOneOrFail(id))
      .pipe(
        map(user => {
          if (!user) {
            throw new NotFoundException(`No user with ID ${id} found!`);
          } else if (user && user.deletedAt) {
            throw new UnprocessableEntityException(`User is already deleted`);
          }
          return user;
        }),
        flatMap(() => this.userRepo.update(id, { deletedAt: new Date(), status: UserStatus.DELETED })),
        map(res => res.raw.affectedRows && res.raw.affectedRows > 0 ? { success: true } : { success: false }),
      );
  }

  onApplicationBootstrap(): void {
    let sub: Subscription;
    const options: CreateUserDto = {
      name: 'Система',
      phone: SYSTEM_USER_PHONE,
      password: 'qwerty123',
    };
    sub = from(this.userRepo.findOne({ phone: options.phone }))
      .pipe(
        filter(u => !u),
        flatMap(() => this.create(options)),
        finalize(() => sub.unsubscribe()),
      )
      .subscribe();
  }
}
