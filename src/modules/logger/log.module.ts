import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Log } from './log.entity';
import { LogService } from './log.service';
import { LogController } from './log.controller';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([Log]), forwardRef(() => AuthModule)],
  providers: [LogService],
  controllers: [LogController],
  exports: [TypeOrmModule, LogService],
})
export class LogModule {
}
