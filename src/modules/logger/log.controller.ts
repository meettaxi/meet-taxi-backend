import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { LogService, SortType } from './log.service';
import { ApiBearerAuth, ApiImplicitQuery, ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from './guards/admin.guard';

@ApiUseTags('logs')
@UseGuards(AuthGuard('jwt'), AdminGuard)
@Controller('log')
export class LogController {
  constructor(private readonly logService: LogService) {
  }

  @Get()
  @ApiBearerAuth()
  @ApiImplicitQuery({ name: 'sort', required: false, enum: ['ASC', 'DESC'] })
  @ApiImplicitQuery({ name: 'limit', required: false })
  @ApiImplicitQuery({ name: 'offset', required: false })
  getAll(@Query('sort') sort?: SortType, @Query('limit') limit?: number, @Query('offset') offset?: number) {
    return this.logService.getAll(limit, offset, sort);
  }
}
