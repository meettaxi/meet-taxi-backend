import { CanActivate, ExecutionContext, ForbiddenException, Injectable } from '@nestjs/common';
import { AuthService } from '../../auth/auth.service';
import { Request } from 'express';
import { JwtPayload } from '../../auth/models/jwt-payload.model';
import { ExtractJwt } from 'passport-jwt';
import { SYSTEM_USER_PHONE } from '../../../common/constants';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private readonly authService: AuthService) {
  }

  canActivate(context: ExecutionContext): boolean {
    const req: Request = context.switchToHttp().getRequest();
    const payload: JwtPayload = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(req));
    if (+payload.phone === SYSTEM_USER_PHONE) {
      return true;
    }
    throw new ForbiddenException(`Only admins can view logs`);
  }
}
