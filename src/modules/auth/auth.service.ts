import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { from, Observable, of, zip } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDto } from './dto/login-user.dto';
import { IUser } from '../user/iuser.entity';
import { JwtPayload } from './models/jwt-payload.model';
import { JwtToken } from './models/jwt-token.model';
import { SESSION_REFRESH_TIME } from '../../common/constants';
import { UserStatus } from '../../common/models/enums';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService, private readonly jwtService: JwtService) {
  }

  validateUser(phone: number, password: string): Observable<IUser | null> {
    return this.userService.getByPhone(phone, true)
      .pipe(
        flatMap(user => zip(from(bcrypt.compare(password, user.password)), of(user))),
        map(([result, user]) => {
          if (result) {
            return user;
          }
          return null;
        }),
      );
  }

  login(loginOptions: LoginUserDto): Observable<string> {
    return this.validateUser(loginOptions.phone, loginOptions.password)
      .pipe(
        map(result => {
          if (!result) {
            throw new UnauthorizedException('Wrong phone/password pair');
          }
          return result;
        }),
        flatMap(user => {
          if (user.status === UserStatus.BANNED) {
            throw new ForbiddenException('Account banned');
          }
          if (user.status === UserStatus.DELETED) {
            throw new ForbiddenException(`Account was deleted at ${user.deletedAt}`);
          }
          const payload: JwtPayload = { phone: user.phone, sub: user.id };
          return from(this.jwtService.signAsync(payload));
        }),
      );
  }

  refresh(token: string): Observable<string> {
    const decodedToken = this.jwtService.decode(token) as JwtToken;
    if (!decodedToken.exp) {
      throw new UnauthorizedException();
    }
    if (Date.now() > decodedToken.exp * 1000 + SESSION_REFRESH_TIME) {
      throw new ForbiddenException(`Maximum refresh time of the session has passed. Please, authorize via /auth/login.`);
    }
    return this.userService.getById(decodedToken.sub)
      .pipe(
        map(user => {
          if (user.status === UserStatus.BANNED) {
            throw new ForbiddenException('Account banned');
          }
          if (user.status === UserStatus.DELETED) {
            throw new ForbiddenException(`Account was deleted at ${user.deletedAt}`);
          }
          return user;
        }),
        flatMap(() => this.jwtService.signAsync({ phone: decodedToken.phone, sub: decodedToken.sub })),
      );
  }

  getPayload(token: string): JwtPayload {
    const { phone, sub } = this.jwtService.decode(token) as Partial<JwtPayload>;
    return { phone, sub };
  }
}
