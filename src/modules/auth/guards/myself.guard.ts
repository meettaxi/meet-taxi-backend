import { CanActivate, ExecutionContext, ForbiddenException, Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { ExtractJwt } from 'passport-jwt';
import { Request } from 'express';
import { JwtPayload } from '../models/jwt-payload.model';

@Injectable()
export class MyselfGuard implements CanActivate {
  constructor(private readonly authService: AuthService) {
  }

  canActivate(context: ExecutionContext): boolean {
    const req: Request = context.switchToHttp().getRequest();
    const payload: JwtPayload = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(req));
    if (+req.params.id === payload.sub) {
      return true;
    }
    throw new ForbiddenException(`You can only edit or delete yourself`);
  }
}
