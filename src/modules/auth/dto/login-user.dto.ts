import { IsNotEmpty, IsNumber, IsString, MaxLength, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH } from '../../../common/constants';

export class LoginUserDto {
  @IsNumber()
  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number', example: 88005553535, nullable: false })
  readonly phone: number;

  @IsString()
  @IsNotEmpty()
  @MinLength(MIN_PASSWORD_LENGTH)
  @MaxLength(MAX_PASSWORD_LENGTH)
  @ApiModelProperty({
    required: true,
    type: 'string',
    nullable: false,
    minLength: MIN_PASSWORD_LENGTH,
    maxLength: MAX_PASSWORD_LENGTH,
    example: 'QwErTy123',
  })
  readonly password: string;
}
