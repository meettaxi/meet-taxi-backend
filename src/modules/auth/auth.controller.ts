import { Body, Controller, HttpCode, InternalServerErrorException, Post, Req, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiUseTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginUserDto } from './dto/login-user.dto';
import { Observable, of } from 'rxjs';
import { Request } from 'express';
import { ExtractJwt } from 'passport-jwt';
import { catchError, map } from 'rxjs/operators';
import { AccessToken } from './models/access-token.model';
import { LoggingInterceptor } from '../logger/interceptors/logging.interceptor';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Post('login')
  @HttpCode(200)
  @ApiOkResponse({ type: AccessToken, description: 'Object with `access_token` to be able to call secured paths' })
  login(@Body() options: LoginUserDto): Observable<AccessToken> {
    return this.authService.login(options)
      .pipe(
        map(token => {
          return { access_token: token };
        }),
      );
  }

  @Post('refresh')
  @ApiBearerAuth()
  @UseInterceptors(LoggingInterceptor)
  @HttpCode(200)
  @ApiOkResponse({ type: AccessToken, description: 'Object with `access_token` to be able to call secured paths' })
  refresh(@Req() req: Request): Observable<AccessToken> {
    return this.authService.refresh(ExtractJwt.fromAuthHeaderAsBearerToken()(req))
      .pipe(
        map(token => {
          return { access_token: token };
        }),
        catchError(err => {
          if (err.response) {
            return of(err.response);
          }
          throw new InternalServerErrorException(err.message);
        }),
      );
  }
}
