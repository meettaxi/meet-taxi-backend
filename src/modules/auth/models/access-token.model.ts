import { ApiModelProperty } from '@nestjs/swagger';

export class AccessToken {
  @ApiModelProperty({
    type: 'string',
    nullable: false,
    description: 'Access token with fields: `sub`, `phone`, `exp`, `iat`. \n`sub` is userID, `phone` is user\'s phone, `exp` - token expiration time, `iat` - token creation time',
    example: 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6Ijg4MDA1NTUzNTM1Iiwic3ViIjoxLCJpYXQiOjE1Nzc2MTg2MjAsImV4cCI6MTU3NzYxOTUyMH0.HLoAZbpZUnVz61C0q7wumTXPWLhD_717P4rYKt-SZjcFQ30WCTxAAbwv8FwfJ_AHxdxd4xhbfysKDdjrEw33ww',
  })
    // tslint:disable-next-line:variable-name
  access_token: string;
}
