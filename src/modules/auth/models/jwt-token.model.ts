import { JwtPayload } from './jwt-payload.model';

/**
 * Decoded JWT token content
 */
export interface JwtToken extends JwtPayload {
  iat: number;
  exp: number;
}
