export interface JwtPayload {
  phone: number;
  sub: number;
}
