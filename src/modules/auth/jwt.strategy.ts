import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IUser } from '../user/iuser.entity';
import { jwtConstants } from '../../common/constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secretKey,
    });
  }

  validate(phone: number, password: string): Observable<IUser> {
    return this.authService.validateUser(phone, password)
      .pipe(
        map(user => {
          if (!user) {
            throw new UnauthorizedException();
          }
          return user;
        }),
      );
  }
}
