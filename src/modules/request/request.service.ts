import { ForbiddenException, Injectable, InternalServerErrorException, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { IRequest } from './irequest.entity';
import { from, Observable, of, zip } from 'rxjs';
import { IUser } from '../user/iuser.entity';
import { IRide } from '../ride/iride.entity';
import { catchError, flatMap, map } from 'rxjs/operators';
import { SimpleUser } from '../user/models/simple-user.model';
import { iRequestToRequest, iUserToSimpleUser } from '../../common/utils';
import { Request } from './models/request.model';
import { UpdateRequestDto } from './dto/update-request.dto';
import { RequestStatus, RideStatus } from '../../common/models/enums';
import { MaximumCompanionsException } from './exceptions/maximum-companions.exception';
import { RideNotOpenException } from './exceptions/ride-not-open.exception';
import { AttemtToJoinOwnRideException } from './exceptions/attemt-to-join-own-ride.exception';
import { UserAlreadyJoinedRideException } from './exceptions/user-already-joined-ride.exception';
import { RequestNotNewException } from './exceptions/request-not-new.exception';
import { DuplicateRequestException } from './exceptions/duplicate-request.exception';
import { iRideRelations } from '../ride/ride.service';
import { MessageService } from '../message/message.service';
import { SYSTEM_USER_PHONE } from '../../common/constants';
import { ICreateRequestDto } from './dto/i-create-request.dto';

@Injectable()
export class RequestService {
  constructor(@InjectRepository(IRequest) private readonly requestRepo: Repository<IRequest>,
              @InjectRepository(IRide) private readonly rideRepo: Repository<IRide>,
              @InjectRepository(IUser) private readonly userRepo: Repository<IUser>,
              private readonly messageService: MessageService) {
  }

  getById(id: number): Observable<Request> {
    return from(this.requestRepo.findOneOrFail(id, { relations: ['author', 'ride'] }))
      .pipe(
        map(req => iRequestToRequest(req)),
      );
  }

  create(options: ICreateRequestDto): Observable<Request> {
    return zip(
      from(this.userRepo.findOneOrFail(options.author, { relations: ['participatedRides'] })),
      from(this.rideRepo.findOneOrFail(options.ride, { relations: ['companions'] })),
    )
      .pipe(
        map(([author, iride]) => {
          if (author.id === iride.createdBy.id) {
            throw new AttemtToJoinOwnRideException();
          }
          if (iride.status !== RideStatus.OPEN) {
            throw new RideNotOpenException(iride.status);
          }
          if (iride.companions.length === iride.maxCompanions) {
            throw new MaximumCompanionsException(iride.maxCompanions);
          }
          iride.companions.forEach(companion => {
            if (companion.id === author.id) {
              throw new UserAlreadyJoinedRideException();
            }
          });
          return [author.id, iride.id];
        }),
        flatMap(([author, ride]) => this.requestRepo.query(`SELECT id FROM i_request WHERE rideId=${ride} AND authorId=${author};`)),
        map(res => {
          if (res && res.length !== 0) {
            throw new DuplicateRequestException();
          }
        }),
        flatMap(() => this.requestRepo.save(options as DeepPartial<IRequest>)),
        flatMap(r => this.getById(r.id)),
        map((req: IRequest) => {
          const author: SimpleUser = iUserToSimpleUser(req.author);
          return Object.assign(req, { author: author });
        }),
      );
  }

  update(id: number, options: UpdateRequestDto): Observable<Request> {
    return from(this.requestRepo.findOneOrFail(id, { relations: ['ride', 'author'] }))
      .pipe(
        map(req => {
          if (req.status !== RequestStatus.NEW) {
            throw new RequestNotNewException();
          }
          return req;
        }),
        flatMap(req => zip(of(req), from(this.rideRepo.findOne(req.ride.id, { relations: iRideRelations })))),
        flatMap(([req, iRide]) => {
          if (options.status === RequestStatus.ACCEPTED) {
            if (iRide.companions.length === iRide.maxCompanions) {
              throw new ForbiddenException('Cannot accept request because target ride has reached maximum of companions');
            } else {
              const ride: IRide = Object.assign(iRide, { companions: [...iRide.companions, req.author] });
              return from(this.rideRepo.save(ride))
                .pipe(
                  flatMap(rd => zip(of(rd), this.userRepo.findOne({ phone: SYSTEM_USER_PHONE }))),
                  flatMap(([rd, sys]) => this.messageService.create({
                    author: sys.id,
                    text: `Пользователь ${req.author.name} присоединился к поездке`,
                    ride: rd.id,
                  })),
                );
            }
          }
          return of();
        }),
        flatMap(() => this.requestRepo.update(id, { status: options.status })),
        flatMap(() => this.getById(id)),
        catchError(err => {
          if (err.status === 422) {
            throw new UnprocessableEntityException(err.response || err);
          }
          throw new InternalServerErrorException(err.response || err);
        }),
      );
  }
}
