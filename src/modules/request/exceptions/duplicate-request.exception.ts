import { HttpStatus, UnprocessableEntityException } from '@nestjs/common';

export class DuplicateRequestException extends UnprocessableEntityException {
  constructor() {
    super({
      message: `User has already applied to this ride`,
      code: 4006,
      status: HttpStatus.UNPROCESSABLE_ENTITY,
    });
  }
}
