import { HttpStatus, UnprocessableEntityException } from '@nestjs/common';

export class RequestNotNewException extends UnprocessableEntityException {
  constructor() {
    super({
      message: 'Request must have status `NEW` to be changed',
      code: 4005,
      status: HttpStatus.UNPROCESSABLE_ENTITY,
    });
  }
}
