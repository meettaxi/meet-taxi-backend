import { ForbiddenException, HttpStatus } from '@nestjs/common';

export class AttemtToJoinOwnRideException extends ForbiddenException {
  constructor() {
    super({
      message: 'Cannot send request to self created ride',
      code: 4001,
      status: HttpStatus.FORBIDDEN,
    });
  }
}
