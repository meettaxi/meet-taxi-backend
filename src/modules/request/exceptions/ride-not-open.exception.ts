import { ForbiddenException, HttpStatus } from '@nestjs/common';
import { RideStatus } from '../../../common/models/enums';

export class RideNotOpenException extends ForbiddenException {
  constructor(status: RideStatus) {
    super({
      message: `Cannot send request to ride with status ${status}`,
      code: 4002,
      status: HttpStatus.FORBIDDEN,
    });
  }
}
