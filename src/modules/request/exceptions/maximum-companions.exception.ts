import { ForbiddenException, HttpStatus } from '@nestjs/common';

export class MaximumCompanionsException extends ForbiddenException {
  constructor(maxCompanions: number) {
    super({
      message: `Cannot send request to ride as it has reached maximum of companions: ${maxCompanions}`,
      code: 4003,
      status: HttpStatus.FORBIDDEN,
    });
  }
}
