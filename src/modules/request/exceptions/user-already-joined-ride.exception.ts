import { ForbiddenException, HttpStatus } from '@nestjs/common';

export class UserAlreadyJoinedRideException extends ForbiddenException {
  constructor() {
    super({
      message: 'Cannot send request to ride because user has already joined this ride',
      code: 4004,
      status: HttpStatus.FORBIDDEN,
    });
  }
}
