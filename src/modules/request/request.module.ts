import { Module } from '@nestjs/common';
import { RequestService } from './request.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IRequest } from './irequest.entity';
import { IUser } from '../user/iuser.entity';
import { IRide } from '../ride/iride.entity';
import { RequestController } from './request.controller';
import { AuthModule } from '../auth/auth.module';
import { MessageModule } from '../message/message.module';
import { LogModule } from '../logger/log.module';

@Module({
  imports: [TypeOrmModule.forFeature([IRequest, IUser, IRide]), AuthModule, MessageModule, LogModule],
  controllers: [RequestController],
  providers: [RequestService],
  exports: [RequestService],
})
export class RequestModule {
}
