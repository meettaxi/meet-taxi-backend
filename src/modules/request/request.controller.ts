import {
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { RequestService } from './request.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiUseTags } from '@nestjs/swagger';
import { CreateRequestDto } from './dto/create-request.dto';
import { Request } from './models/request.model';
import { UpdateRequestDto } from './dto/update-request.dto';
import { AuthGuard } from '@nestjs/passport';
import { RequestGuard } from './guards/request.guard';
import { ExtractJwt } from 'passport-jwt';
import { AuthService } from '../auth/auth.service';
import { Request as HttpRequest } from 'express';
import { LoggingInterceptor } from '../logger/interceptors/logging.interceptor';

@ApiUseTags('request')
@UseInterceptors(LoggingInterceptor)
@Controller('request')
export class RequestController {
  constructor(private readonly requestService: RequestService,
              private readonly authService: AuthService) {
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOkResponse({ type: Request })
  getById(@Param('id') id: number): Observable<Request> {
    return this.requestService.getById(id)
      .pipe(
        catchError(err => {
          if (err.name === 'EntityNotFound') {
            throw new NotFoundException(`No ride found with specified ID: ${id}`);
          }
          throw new InternalServerErrorException(err);
        }),
      );
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'), RequestGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: Request })
  update(@Body() options: UpdateRequestDto, @Param('id') id: number): Observable<Request> {
    return this.requestService.update(id, options);
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiCreatedResponse({ type: Request })
  @ApiBearerAuth()
  create(@Body() createOptions: CreateRequestDto, @Req() req: HttpRequest): Observable<Request> {
    const author: number = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(req)).sub;
    return this.requestService.create({ ...createOptions, author: author });
  }
}
