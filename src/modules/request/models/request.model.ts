import { RequestStatus, RideStatus } from '../../../common/models/enums';
import { SimpleUser } from '../../user/models/simple-user.model';
import { ApiModelProperty } from '@nestjs/swagger';
import { SimpleRide } from '../../ride/models/simple-ride.model';

export class Request {
  @ApiModelProperty({ type: 'integer', nullable: false, required: true, example: 1 })
  id: number;

  @ApiModelProperty({ type: 'string', nullable: true, required: false, example: 'Will be with 2 bags' })
  text?: string;

  @ApiModelProperty({ type: RequestStatus, nullable: false, required: true, example: 'CANCELED' })
  status: RequestStatus;

  @ApiModelProperty({
    type: SimpleRide,
    nullable: false,
    required: true,
    example: { id: 1, status: RideStatus.OPEN, departureDate: '2019-12-01T16:11:53.000Z', createdBy: { id: 2, name: 'Ivan', phone: 1000 } },
  })
  ride: SimpleRide;

  @ApiModelProperty({ type: SimpleUser, nullable: false, required: true, example: { id: 1, name: 'Ivan', phone: 9000 } })
  author: SimpleUser;
}
