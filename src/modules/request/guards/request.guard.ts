import { CanActivate, ExecutionContext, ForbiddenException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { RequestService } from '../request.service';
import { Request } from 'express';
import { JwtPayload } from '../../auth/models/jwt-payload.model';
import { ExtractJwt } from 'passport-jwt';
import { catchError, map } from 'rxjs/operators';
import { RequestStatus } from '../../../common/models/enums';

@Injectable()
export class RequestGuard implements CanActivate {
  constructor(private readonly authService: AuthService,
              private readonly requestService: RequestService) {
  }

  canActivate(context: ExecutionContext): boolean | Observable<boolean> {
    const httpRequest: Request = context.switchToHttp().getRequest();
    const payload: JwtPayload = this.authService.getPayload(ExtractJwt.fromAuthHeaderAsBearerToken()(httpRequest));
    if (httpRequest.params.id && !isNaN(+httpRequest.params.id)) {
      return this.requestService.getById(+httpRequest.params.id)
        .pipe(
          catchError(err => {
            throw new InternalServerErrorException(err);
          }),
          map(req => {
            const status: RequestStatus = httpRequest.body.status;
            if ((req.ride.createdBy.id === payload.sub && (status === RequestStatus.ACCEPTED || status === RequestStatus.DECLINED)) ||
              (payload.sub === req.author.id && status === RequestStatus.CANCELED)) {
              return true;
            }
            if (status === RequestStatus.CANCELED) {
              throw new ForbiddenException('Only request`s creator can cancel request.');
            }
            throw new ForbiddenException('Only ride`s creator can accept/decline join requests.');
          }),
        );
    }
    return false;
  }
}
