import { RequestStatus } from '../../../common/models/enums';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateRequestDto {
  @ApiModelProperty({
    type: 'string',
    enum: ['ACCEPTED', 'DECLINED', 'CANCELED'],
    required: true,
    nullable: false,
    example: 'CANCELED',
  })
  status: RequestStatus;
}
