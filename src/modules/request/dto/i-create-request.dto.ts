import { CreateRequestDto } from './create-request.dto';
import { IsInt, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ICreateRequestDto extends CreateRequestDto {
  @IsInt()
  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'integer', nullable: false, example: 1 })
  author: number;
}
