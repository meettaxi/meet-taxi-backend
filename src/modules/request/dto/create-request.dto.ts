import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateRequestDto {
  @IsInt()
  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'integer', nullable: false, example: 1 })
  ride: number;

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @ApiModelPropertyOptional({ type: 'string', nullable: true, example: 'Буду с 2 чемоданами. Возьмёте?' })
  text?: string;
}
