import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { RequestStatus } from '../../common/models/enums';
import { IRide } from '../ride/iride.entity';
import { IUser } from '../user/iuser.entity';

@Entity()
export class IRequest {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @Column({ type: 'varchar', nullable: true })
  text: string;

  @Column({ type: 'enum', enum: RequestStatus, default: RequestStatus.NEW })
  status: RequestStatus;

  @ManyToOne(type => IRide, ride => ride.requests, { eager: true })
  ride: IRide;

  @ManyToOne(type => IUser, user => user.requests, { eager: true })
  author: IUser;
}
