import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as helmet from 'helmet';
import { ValidationPipe } from '@nestjs/common';
import { API_VERSION } from './common/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
  const options = new DocumentBuilder()
    .setTitle('MeetTaxi API')
    .setDescription('RESTful API of MeetTaxi service')
    .setVersion(`${API_VERSION}`)
    .addTag('user')
    .addTag('ride')
    .addTag('request')
    .addTag('message')
    .addTag('auth')
    .addTag('logs')
    .setContactEmail('meshkov.ra@ya.ru')
    .addBearerAuth('Authorization', 'header')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}

bootstrap();
