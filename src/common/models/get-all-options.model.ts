import { SortType } from './enums';
import { IsEnum, IsNotEmpty, IsNumberString, IsOptional } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class GetAllOptions {
  @IsOptional()
  @IsNumberString()
  @ApiModelPropertyOptional({
    type: 'integer',
    minimum: 0,
    example: 10,
    default: 0,
    description: 'Offset from the start of the result list',
  })
  offset?: number;

  @IsNumberString()
  @IsOptional()
  @ApiModelPropertyOptional({ type: 'integer', minimum: 1, example: 5, default: 15, description: 'Maximum entities that will be output' })
  limit?: number;

  @IsEnum(Object.values(SortType))
  @IsNotEmpty()
  @IsOptional()
  @ApiModelPropertyOptional({ type: SortType, enum: Object.values(SortType), example: SortType.ASC, default: SortType.DESC })
  sort?: SortType;
}
