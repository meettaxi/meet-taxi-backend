import { IsLatitude, IsLongitude, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class Coordinates {
  @IsLatitude()
  @IsNotEmpty()
  @ApiModelProperty({ required: true, nullable: false, type: 'number', example: 55.3246 })
  latitude: number;

  @IsLongitude()
  @IsNotEmpty()
  @ApiModelProperty({ required: true, nullable: false, type: 'number', example: 37.309 })
  longitude: number;
}
