import { ApiModelProperty } from '@nestjs/swagger';

export class DeleteResult {
  @ApiModelProperty({ required: true, readOnly: true, type: 'boolean', example: true, description: 'Shows if the deletion is successful' })
  success: boolean;
}
