export enum UserStatus {
  ACTIVE = 'ACTIVE',
  BANNED = 'BANNED',
  DELETED = 'DELETED',
}

export enum RideStatus {
  OPEN = 'OPEN',
  STARTED = 'STARTED',
  FINISHED = 'FINISHED',
  CANCELED = 'CANCELED',
}

export enum RequestStatus {
  NEW = 'NEW',
  ACCEPTED = 'ACCEPTED',
  DECLINED = 'DECLINED',
  CANCELED = 'CANCELED',
}

export enum SortType {
  ASC = 'ASC',
  DESC = 'DESC',
}
