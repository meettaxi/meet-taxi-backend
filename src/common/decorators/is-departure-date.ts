import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';
import { MIN_RIDE_CREATION_INTERVAL } from '../constants';

export function IsDepartureDate(validationOptions?: ValidationOptions) {
  return function(object: any, propertyName: string) {
    registerDecorator({
      name: 'IsDepartureDate',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          return new Date(value).getTime() >= Date.now() + MIN_RIDE_CREATION_INTERVAL;
        },
      },
    });
  };
}
