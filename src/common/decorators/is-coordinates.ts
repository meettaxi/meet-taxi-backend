import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function IsCoordinates(validationOptions?: ValidationOptions) {
  return function(object: any, propertyName: string) {
    registerDecorator({
      name: 'IsCoordinates',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          return value.latitude && value.longitude && value.latitude >= -90 && value.latitude <= 90 &&
            value.longitude >= -180 && value.longitude <= 180;
        },
      },
    });
  };
}
