// Common
export const API_VERSION: number = 0.8;

// User
export const MIN_NAME_LENGTH: number = 2;
export const MAX_NAME_LENGTH: number = 100;
export const MIN_PASSWORD_LENGTH: number = 6;
export const MAX_PASSWORD_LENGTH: number = 50;
export const SYSTEM_USER_PHONE: number = 111;

// Auth
/**
 * Session duration, seconds
 */
export const SESSION_DURATION: number = 900;
export const SESSION_REFRESH_TIME: number = 86400000; // 24 hours
export const jwtConstants = {
  secretKey: process.env.NODE_ENV === 'production' ? process.env.SECRET_KEY : 'secretKey',
};

// Message
export const MAX_MESSAGE_TEXT_LENGTH: number = 2000;

// Ride
export const MAX_RIDE_COMPANIONS: number = 3;
export const MIN_RIDE_COMPANIONS: number = 1;
export const MAX_RIDE_COMMENT_LENGTH: number = 2000;
export const RIDE_STARTED_SYS_MESSAGE: string = 'Поездка началась';
export const RIDE_FINISHED_SYS_MESSAGE: string = 'Поездка завершена. Спасибо за поездку!';
/**
 * Minimum interval (ms) from now when you can create a ride (12 hours)
 */
export const MIN_RIDE_CREATION_INTERVAL: number = 43200000;
/**
 * Minimum interval (ms) from departureDate when the ride can be marked as finished (5 hours)
 */
export const RIDE_FINISHED_INTERVAL: number = 18000000;
