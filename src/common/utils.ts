/**
 * Convert rating field from IUser model to User model
 * @param rating
 */
import { Rating } from '../modules/user/models/rating.model';
import { IUser } from '../modules/user/iuser.entity';
import { User } from '../modules/user/models/user.model';
import { SimpleUser } from '../modules/user/models/simple-user.model';
import { IRide } from '../modules/ride/iride.entity';
import { Ride } from '../modules/ride/models/ride.model';
import { IMessage } from '../modules/message/imessage.entity';
import { Message } from '../modules/message/models/message.model';
import { SimpleRide } from '../modules/ride/models/simple-ride.model';
import { IRequest } from '../modules/request/irequest.entity';
import { Request } from '../modules/request/models/request.model';

export function convertRating(rating: Rating): number | null {
  return rating && rating.marksCount ? +(rating.sum / rating.marksCount).toFixed(2) : null;
}

export function addRatingMark(rating: Rating, mark: number): Rating {
  if (!rating) { // if it is the first mark
    rating = { marksCount: 0, sum: 0 };
  }
  return {
    marksCount: rating.marksCount + 1,
    sum: mark + rating.sum,
  };
}

export function iUserToUser(iUser: IUser): User | undefined {
  return iUser ? Object.assign(iUser, {
    rating: convertRating(iUser.rating),
    requests: iUser.requests.map(r => iRequestToRequest(r)),
    createdRides: iUser.createdRides.map(rd => iRideToRide(rd)),
    participatedRides: iUser.participatedRides.map(rd => iRideToRide(rd)),
    messages: iUser.messages.map(m => iMessageToMessage(m)),
    phone: +iUser.phone,
  }) : undefined;
}

export function iUserToSimpleUser(iUser: IUser): SimpleUser {
  return iUser ? {
    id: iUser.id,
    name: iUser.name,
    phone: +iUser.phone,
  } : undefined;
}

export function iRideToRide(iRide: IRide): Ride | undefined {
  return iRide ? {
    id: iRide.id,
    destinationPoint: iRide.destinationPoint,
    departureDate: iRide.departureDate,
    createdBy: iUserToSimpleUser(iRide.createdBy),
    createdAt: iRide.createdAt,
    companions: iRide.companions ? iRide.companions.map(u => iUserToSimpleUser(u)) : undefined,
    comment: iRide.comment,
    departurePoint: iRide.departurePoint,
    status: iRide.status,
    requests: iRide.requests ? iRide.requests.map(r => iRequestToRequest(r)) : undefined,
    messages: iRide.messages,
    departureAddress: iRide.departureAddress,
    destinationAddress: iRide.destinationAddress,
  } : undefined;
}

export function iRideToSimpleRide(iRide: IRide): SimpleRide | undefined {
  return iRide ? {
    id: iRide.id,
    createdBy: iUserToSimpleUser(iRide.createdBy),
    status: iRide.status,
    departureDate: iRide.departureDate,
    departureAddress: iRide.departureAddress,
    destinationAddress: iRide.destinationAddress,
  } : undefined;
}

export function iMessageToMessage(iMessage: IMessage): Message | undefined {
  return iMessage ? {
    id: iMessage.id,
    isEdited: iMessage.isEdited,
    author: iUserToSimpleUser(iMessage.author),
    postedAt: iMessage.postedAt,
    ride: iRideToSimpleRide(iMessage.ride),
    text: iMessage.text,
  } : undefined;
}

export function iRequestToRequest(iRequest: IRequest): Request | undefined {
  return iRequest ? {
    id: iRequest.id,
    author: iUserToSimpleUser(iRequest.author),
    ride: iRequest.ride ? iRideToSimpleRide(iRequest.ride) : null,
    text: iRequest.text,
    status: iRequest.status,
  } : undefined;
}
