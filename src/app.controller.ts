import { Controller, Get } from '@nestjs/common';
import { API_VERSION } from './common/constants';

@Controller()
export class AppController {

  @Get()
  getStatus() {
    return { status: 'ok', apiVersion: API_VERSION };
  }
}
